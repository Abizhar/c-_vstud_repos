/*
 * Project name:
     UART example using interrupt
 * Copyright:
   
 * Description:
      initial UART using Usart_init(), enable RCIE for interrupt
      store incoming data in Rx_buff (maximim 31 characters)
      ,no error check,inside ISR untill timeout occur then send 
      it back from main()
 * Test configuration:
     MCU:             PIC16F886
     Dev.Board:       -
     Oscillator:      HS, 16.0000 MHz
     Ext. Modules:    -
     SW:              mikroC v8.2.0.0
 * NOTES:

*/
char Rx_buff[32];
char New_Flag = 0;
char *Rx_buff_ptr;
void interrupt(){
     char i = 0;
     unsigned timeOut = 0;
     if (PIR1.RCIF) {
         while (timeOut < 20000){
              if (Usart_Data_Ready()){
                 Rx_buff[i] = Usart_Read();
                 i++;
                 Rx_buff[i] = 0; // Add NULL
                 timeOut = 0;    // reset timeout
              }
              timeOut++;
              }
         New_Flag = 1;
     }
}



void main(){

     TRISB = 0;
     PORTB = 0;
     ANSELH = 0;
     INTCON.GIE = 1;
     INTCON.PEIE = 1;
     PIE1.RCIE=1;    //enable receive interrupt
     Usart_init(9600);
     while(1){
          if(New_Flag){
               New_Flag = 0;
               Rx_buff_ptr = &Rx_buff[0];
               while(*Rx_buff_ptr) {
               Usart_Write(*Rx_buff_ptr);
               Rx_buff_ptr++;
               }
          }
     }
}