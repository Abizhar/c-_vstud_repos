
_interrupt:

;KP.c,13 :: 		void interrupt() {
;KP.c,14 :: 		USB_Interrupt_Proc();
	CALL        _USB_Interrupt_Proc+0, 0
;KP.c,15 :: 		TMR0L = 100;       //Reload Value
	MOVLW       100
	MOVWF       TMR0L+0 
;KP.c,16 :: 		INTCON.TMR0IF = 0;    //Re-Enable Timer-0 Interrupt
	BCF         INTCON+0, 2 
;KP.c,17 :: 		}
L_end_interrupt:
L__interrupt28:
	RETFIE      1
; end of _interrupt

_UART1_Write_Text_Newline:

;KP.c,20 :: 		void UART1_Write_Text_Newline(unsigned char msg[]) {
;KP.c,21 :: 		UART1_Write_Text(msg);
	MOVF        FARG_UART1_Write_Text_Newline_msg+0, 0 
	MOVWF       FARG_UART1_Write_Text_uart_text+0 
	MOVF        FARG_UART1_Write_Text_Newline_msg+1, 0 
	MOVWF       FARG_UART1_Write_Text_uart_text+1 
	CALL        _UART1_Write_Text+0, 0
;KP.c,22 :: 		UART1_Write(10);
	MOVLW       10
	MOVWF       FARG_UART1_Write_data_+0 
	CALL        _UART1_Write+0, 0
;KP.c,23 :: 		UART1_Write(13);
	MOVLW       13
	MOVWF       FARG_UART1_Write_data_+0 
	CALL        _UART1_Write+0, 0
;KP.c,24 :: 		}
L_end_UART1_Write_Text_Newline:
	RETURN      0
; end of _UART1_Write_Text_Newline

_clear_buffer:

;KP.c,25 :: 		void clear_buffer(unsigned char buffer[]) {
;KP.c,26 :: 		unsigned int i = 0;
	CLRF        clear_buffer_i_L0+0 
	CLRF        clear_buffer_i_L0+1 
;KP.c,27 :: 		while (buffer[i] != '\0') {
L_clear_buffer0:
	MOVF        clear_buffer_i_L0+0, 0 
	ADDWF       FARG_clear_buffer_buffer+0, 0 
	MOVWF       FSR0 
	MOVF        clear_buffer_i_L0+1, 0 
	ADDWFC      FARG_clear_buffer_buffer+1, 0 
	MOVWF       FSR0H 
	MOVF        POSTINC0+0, 0 
	XORLW       0
	BTFSC       STATUS+0, 2 
	GOTO        L_clear_buffer1
;KP.c,28 :: 		buffer[i] = '\0';
	MOVF        clear_buffer_i_L0+0, 0 
	ADDWF       FARG_clear_buffer_buffer+0, 0 
	MOVWF       FSR1 
	MOVF        clear_buffer_i_L0+1, 0 
	ADDWFC      FARG_clear_buffer_buffer+1, 0 
	MOVWF       FSR1H 
	CLRF        POSTINC1+0 
;KP.c,29 :: 		i++;
	INFSNZ      clear_buffer_i_L0+0, 1 
	INCF        clear_buffer_i_L0+1, 1 
;KP.c,30 :: 		}
	GOTO        L_clear_buffer0
L_clear_buffer1:
;KP.c,31 :: 		}
L_end_clear_buffer:
	RETURN      0
; end of _clear_buffer

_main:

;KP.c,33 :: 		void main() {
;KP.c,34 :: 		writebuff[0] = 15;
	MOVLW       15
	MOVWF       1518 
;KP.c,35 :: 		Write_Buffer[0] = 15;
	MOVLW       15
	MOVWF       1349 
;KP.c,36 :: 		writebufflagi[0] = 16;
	MOVLW       16
	MOVWF       1454 
;KP.c,37 :: 		UART1_Init(9600);
	BSF         BAUDCON+0, 3, 0
	MOVLW       4
	MOVWF       SPBRGH+0 
	MOVLW       225
	MOVWF       SPBRG+0 
	BSF         TXSTA+0, 2, 0
	CALL        _UART1_Init+0, 0
;KP.c,38 :: 		ADCON1 |= 0x0F;          // Configure AN pins as digital
	MOVLW       15
	IORWF       ADCON1+0, 1 
;KP.c,39 :: 		CMCON |= 7;            // Disable comparators
	MOVLW       7
	IORWF       CMCON+0, 1 
;KP.c,40 :: 		TRISB = 0b00000000;
	CLRF        TRISB+0 
;KP.c,41 :: 		TRISC = 0x80;
	MOVLW       128
	MOVWF       TRISC+0 
;KP.c,42 :: 		TRISD = 0b00000000; //input 1,output 0
	CLRF        TRISD+0 
;KP.c,43 :: 		LATD5_bit = 0;             //RE
	BCF         LATD5_bit+0, BitPos(LATD5_bit+0) 
;KP.c,44 :: 		LATD4_bit = 1;                 //DE
	BSF         LATD4_bit+0, BitPos(LATD4_bit+0) 
;KP.c,46 :: 		INTCON = 0;
	CLRF        INTCON+0 
;KP.c,47 :: 		INTCON2 = 0xF5;
	MOVLW       245
	MOVWF       INTCON2+0 
;KP.c,48 :: 		INTCON3 = 0xC0;
	MOVLW       192
	MOVWF       INTCON3+0 
;KP.c,49 :: 		RCON.IPEN = 0;
	BCF         RCON+0, 7 
;KP.c,50 :: 		PIE1 = 0;
	CLRF        PIE1+0 
;KP.c,51 :: 		PIE2 = 0;
	CLRF        PIE2+0 
;KP.c,52 :: 		PIR1 = 0;
	CLRF        PIR1+0 
;KP.c,53 :: 		PIR2 = 0;
	CLRF        PIR2+0 
;KP.c,60 :: 		T0CON = 0x47; // Prescaler = 256
	MOVLW       71
	MOVWF       T0CON+0 
;KP.c,61 :: 		TMR0L = 100; // Timer count is 256-156 = 100
	MOVLW       100
	MOVWF       TMR0L+0 
;KP.c,62 :: 		INTCON.TMR0IE = 1; // Enable T0IE
	BSF         INTCON+0, 5 
;KP.c,63 :: 		T0CON.TMR0ON = 1; // Turn Timer 0 ON
	BSF         T0CON+0, 7 
;KP.c,64 :: 		INTCON = 0xE0; // Enable interrupts
	MOVLW       224
	MOVWF       INTCON+0 
;KP.c,66 :: 		Hid_Enable(&Read_Buffer, &Write_Buffer);
	MOVLW       _Read_Buffer+0
	MOVWF       FARG_HID_Enable_readbuff+0 
	MOVLW       hi_addr(_Read_Buffer+0)
	MOVWF       FARG_HID_Enable_readbuff+1 
	MOVLW       _Write_Buffer+0
	MOVWF       FARG_HID_Enable_writebuff+0 
	MOVLW       hi_addr(_Write_Buffer+0)
	MOVWF       FARG_HID_Enable_writebuff+1 
	CALL        _HID_Enable+0, 0
;KP.c,71 :: 		Write_Buffer[0] = 0;
	CLRF        1349 
;KP.c,72 :: 		Read_Buffer[0] = 0;
	CLRF        1280 
;KP.c,73 :: 		writebuff[0] = 0;
	CLRF        1518 
;KP.c,74 :: 		writebufflagi[0] = 0;
	CLRF        1454 
;KP.c,76 :: 		while (1) {
L_main2:
;KP.c,77 :: 		if (UART_Data_Ready()) {
	CALL        _UART_Data_Ready+0, 0
	MOVF        R0, 1 
	BTFSC       STATUS+0, 2 
	GOTO        L_main4
;KP.c,78 :: 		if(tanda==0){
	MOVLW       0
	XORWF       _tanda+1, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main32
	MOVLW       0
	XORWF       _tanda+0, 0 
L__main32:
	BTFSS       STATUS+0, 2 
	GOTO        L_main5
;KP.c,79 :: 		Write_Buffer[data_counter+1] = UART_Read();
	MOVLW       1
	ADDWF       _data_counter+0, 0 
	MOVWF       R0 
	MOVLW       0
	ADDWFC      _data_counter+1, 0 
	MOVWF       R1 
	MOVLW       _Write_Buffer+0
	ADDWF       R0, 0 
	MOVWF       FLOC__main+0 
	MOVLW       hi_addr(_Write_Buffer+0)
	ADDWFC      R1, 0 
	MOVWF       FLOC__main+1 
	CALL        _UART_Read+0, 0
	MOVFF       FLOC__main+0, FSR1
	MOVFF       FLOC__main+1, FSR1H
	MOVF        R0, 0 
	MOVWF       POSTINC1+0 
;KP.c,80 :: 		data_counter++;
	INFSNZ      _data_counter+0, 1 
	INCF        _data_counter+1, 1 
;KP.c,81 :: 		if (data_counter > 62) {
	MOVLW       0
	MOVWF       R0 
	MOVF        _data_counter+1, 0 
	SUBWF       R0, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main33
	MOVF        _data_counter+0, 0 
	SUBLW       62
L__main33:
	BTFSC       STATUS+0, 0 
	GOTO        L_main6
;KP.c,82 :: 		data_counter1 = 0;
	CLRF        _data_counter1+0 
	CLRF        _data_counter1+1 
;KP.c,83 :: 		tanda = 1;
	MOVLW       1
	MOVWF       _tanda+0 
	MOVLW       0
	MOVWF       _tanda+1 
;KP.c,84 :: 		}
L_main6:
;KP.c,86 :: 		}else if(tanda==1){
	GOTO        L_main7
L_main5:
	MOVLW       0
	XORWF       _tanda+1, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main34
	MOVLW       1
	XORWF       _tanda+0, 0 
L__main34:
	BTFSS       STATUS+0, 2 
	GOTO        L_main8
;KP.c,87 :: 		writebufflagi[data_counter1+1] = UART_Read();
	MOVLW       1
	ADDWF       _data_counter1+0, 0 
	MOVWF       R0 
	MOVLW       0
	ADDWFC      _data_counter1+1, 0 
	MOVWF       R1 
	MOVLW       _writebufflagi+0
	ADDWF       R0, 0 
	MOVWF       FLOC__main+0 
	MOVLW       hi_addr(_writebufflagi+0)
	ADDWFC      R1, 0 
	MOVWF       FLOC__main+1 
	CALL        _UART_Read+0, 0
	MOVFF       FLOC__main+0, FSR1
	MOVFF       FLOC__main+1, FSR1H
	MOVF        R0, 0 
	MOVWF       POSTINC1+0 
;KP.c,88 :: 		data_counter1++;
	INFSNZ      _data_counter1+0, 1 
	INCF        _data_counter1+1, 1 
;KP.c,89 :: 		if (data_counter1 > 62) {
	MOVLW       0
	MOVWF       R0 
	MOVF        _data_counter1+1, 0 
	SUBWF       R0, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main35
	MOVF        _data_counter1+0, 0 
	SUBLW       62
L__main35:
	BTFSC       STATUS+0, 0 
	GOTO        L_main9
;KP.c,90 :: 		data_counter = 0;
	CLRF        _data_counter+0 
	CLRF        _data_counter+1 
;KP.c,91 :: 		tanda = 0;
	CLRF        _tanda+0 
	CLRF        _tanda+1 
;KP.c,92 :: 		}
L_main9:
;KP.c,94 :: 		}
L_main8:
L_main7:
;KP.c,96 :: 		}
L_main4:
;KP.c,98 :: 		if (Hid_Read()) {
	CALL        _HID_Read+0, 0
	MOVF        R0, 1 
	BTFSC       STATUS+0, 2 
	GOTO        L_main10
;KP.c,99 :: 		switch(Read_Buffer[0]){
	GOTO        L_main11
;KP.c,100 :: 		case 0x81:
L_main13:
;KP.c,101 :: 		if(tanda == 1){
	MOVLW       0
	XORWF       _tanda+1, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main36
	MOVLW       1
	XORWF       _tanda+0, 0 
L__main36:
	BTFSS       STATUS+0, 2 
	GOTO        L_main14
;KP.c,102 :: 		Write_Buffer[0] = (unsigned char) data_counter;
	MOVF        _data_counter+0, 0 
	MOVWF       1349 
;KP.c,103 :: 		data_counter=0;
	CLRF        _data_counter+0 
	CLRF        _data_counter+1 
;KP.c,104 :: 		tanda = 0;
	CLRF        _tanda+0 
	CLRF        _tanda+1 
;KP.c,105 :: 		while (!HID_Write(&Write_Buffer, 64));
L_main15:
	MOVLW       _Write_Buffer+0
	MOVWF       FARG_HID_Write_writebuff+0 
	MOVLW       hi_addr(_Write_Buffer+0)
	MOVWF       FARG_HID_Write_writebuff+1 
	MOVLW       64
	MOVWF       FARG_HID_Write_len+0 
	CALL        _HID_Write+0, 0
	MOVF        R0, 1 
	BTFSS       STATUS+0, 2 
	GOTO        L_main16
	GOTO        L_main15
L_main16:
;KP.c,106 :: 		}else if(tanda == 0){
	GOTO        L_main17
L_main14:
	MOVLW       0
	XORWF       _tanda+1, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main37
	MOVLW       0
	XORWF       _tanda+0, 0 
L__main37:
	BTFSS       STATUS+0, 2 
	GOTO        L_main18
;KP.c,107 :: 		writebufflagi[0] = (unsigned char) data_counter1;
	MOVF        _data_counter1+0, 0 
	MOVWF       1454 
;KP.c,108 :: 		data_counter1=0;
	CLRF        _data_counter1+0 
	CLRF        _data_counter1+1 
;KP.c,109 :: 		tanda = 1;
	MOVLW       1
	MOVWF       _tanda+0 
	MOVLW       0
	MOVWF       _tanda+1 
;KP.c,110 :: 		while (!HID_Write(&writebufflagi, 64));
L_main19:
	MOVLW       _writebufflagi+0
	MOVWF       FARG_HID_Write_writebuff+0 
	MOVLW       hi_addr(_writebufflagi+0)
	MOVWF       FARG_HID_Write_writebuff+1 
	MOVLW       64
	MOVWF       FARG_HID_Write_len+0 
	CALL        _HID_Write+0, 0
	MOVF        R0, 1 
	BTFSS       STATUS+0, 2 
	GOTO        L_main20
	GOTO        L_main19
L_main20:
;KP.c,111 :: 		}
L_main18:
L_main17:
;KP.c,112 :: 		break;
	GOTO        L_main12
;KP.c,114 :: 		case 0x82:
L_main21:
;KP.c,115 :: 		writebuff[0] = 10;
	MOVLW       10
	MOVWF       1518 
;KP.c,116 :: 		while(!HID_Write(&writebuff,64));
L_main22:
	MOVLW       _writebuff+0
	MOVWF       FARG_HID_Write_writebuff+0 
	MOVLW       hi_addr(_writebuff+0)
	MOVWF       FARG_HID_Write_writebuff+1 
	MOVLW       64
	MOVWF       FARG_HID_Write_len+0 
	CALL        _HID_Write+0, 0
	MOVF        R0, 1 
	BTFSS       STATUS+0, 2 
	GOTO        L_main23
	GOTO        L_main22
L_main23:
;KP.c,117 :: 		clear_buffer(writebuff);
	MOVLW       _writebuff+0
	MOVWF       FARG_clear_buffer_buffer+0 
	MOVLW       hi_addr(_writebuff+0)
	MOVWF       FARG_clear_buffer_buffer+1 
	CALL        _clear_buffer+0, 0
;KP.c,118 :: 		putar = Read_Buffer[1];
	MOVF        1281, 0 
	MOVWF       _putar+0 
	MOVLW       0
	MOVWF       _putar+1 
;KP.c,123 :: 		for(terima=0;terima<putar;terima++){
	CLRF        _terima+0 
	CLRF        _terima+1 
L_main24:
	MOVLW       128
	XORWF       _terima+1, 0 
	MOVWF       R0 
	MOVLW       128
	XORWF       _putar+1, 0 
	SUBWF       R0, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main38
	MOVF        _putar+0, 0 
	SUBWF       _terima+0, 0 
L__main38:
	BTFSC       STATUS+0, 0 
	GOTO        L_main25
;KP.c,124 :: 		writebuff[terima] = Read_Buffer[terima+2];
	MOVLW       _writebuff+0
	ADDWF       _terima+0, 0 
	MOVWF       FSR1 
	MOVLW       hi_addr(_writebuff+0)
	ADDWFC      _terima+1, 0 
	MOVWF       FSR1H 
	MOVLW       2
	ADDWF       _terima+0, 0 
	MOVWF       R0 
	MOVLW       0
	ADDWFC      _terima+1, 0 
	MOVWF       R1 
	MOVLW       _Read_Buffer+0
	ADDWF       R0, 0 
	MOVWF       FSR0 
	MOVLW       hi_addr(_Read_Buffer+0)
	ADDWFC      R1, 0 
	MOVWF       FSR0H 
	MOVF        POSTINC0+0, 0 
	MOVWF       POSTINC1+0 
;KP.c,123 :: 		for(terima=0;terima<putar;terima++){
	INFSNZ      _terima+0, 1 
	INCF        _terima+1, 1 
;KP.c,125 :: 		}
	GOTO        L_main24
L_main25:
;KP.c,127 :: 		UART1_Write_Text(writebuff);
	MOVLW       _writebuff+0
	MOVWF       FARG_UART1_Write_Text_uart_text+0 
	MOVLW       hi_addr(_writebuff+0)
	MOVWF       FARG_UART1_Write_Text_uart_text+1 
	CALL        _UART1_Write_Text+0, 0
;KP.c,134 :: 		break;
	GOTO        L_main12
;KP.c,135 :: 		}
L_main11:
	MOVF        1280, 0 
	XORLW       129
	BTFSC       STATUS+0, 2 
	GOTO        L_main13
	MOVF        1280, 0 
	XORLW       130
	BTFSC       STATUS+0, 2 
	GOTO        L_main21
L_main12:
;KP.c,137 :: 		}
L_main10:
;KP.c,139 :: 		}
	GOTO        L_main2
;KP.c,143 :: 		}
L_end_main:
	GOTO        $+0
; end of _main
