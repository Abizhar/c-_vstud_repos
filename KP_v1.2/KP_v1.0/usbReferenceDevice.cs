﻿//-----------------------------------------------------------------------------
//
//  usbReferenceDevice.cs
//
//  USB Generic HID Communications 3_0_0_0
//
//  A reference test application for the usbGenericHidCommunications library
//  Copyright (C) 2011 Simon Inns
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//  Web:    http://www.waitingforfriday.com
//  Email:  simon.inns@gmail.com
//
//-----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

// The following namespace allows debugging output (when compiled in debug mode)
using System.Diagnostics;

namespace USB_Generic_HID_reference_application {
    using usbGenericHidCommunications;
    class usbReferenceDevice : usbGenericHidCommunication {
        public usbReferenceDevice(int vid, int pid) : base(vid, pid) {}

		public int HEXstringtoByte(ref string input, ref Byte[] output) {
			//parsing HEX
			while(input[0] < 0x30 || input[0] > 0x66) {
				input = input.Remove(0,1);
				if(input.Length < 1)
					break;
			}

			if(input.Length > 1) {
				while(input[input.Length - 1] < 0x30 || input[input.Length - 1] > 0x66) {
					input = input.Remove(input.Length - 1, 1);
					if(input.Length < 1)
						break;
				}
			}

			if(input.Length > 1) {
				string[] hexValuesSplit = input.Split(' ');

				int i = 0;
				foreach(String hex in hexValuesSplit) {
					try {
						output[i + 3] = (byte)Convert.ToInt32(hex, 16);
						i++;
					}
					catch(Exception e) {
						return -1;
					}
				}

				return i;
			}
			else return 0;
		}

		//hanya terima string dengan panjang 62 karakter
		public int single_packet_send(ref Byte[] inputReportBuffer, ref string inputstr, int size,ref Byte[] outputBuffer) {
			//Byte[] outputBuffer = new Byte[65];

			//input string ke byte array
			inputstr = inputstr.Insert(0, "XXX");
			inputstr = inputstr.PadRight(65, ' ');

			outputBuffer = Encoding.UTF8.GetBytes(inputstr);

			//input command & size
			outputBuffer[0] = 0;
			outputBuffer[1] = 0x82;
			outputBuffer[2] = (byte)size;

			// <--checksum code here
			//write buffer to device
			if(writeRawReportToDevice(outputBuffer) && readSingleReportFromDevice(ref inputReportBuffer)) {
				int data_count = (int)inputReportBuffer[1];
				inputReportBuffer[1] = 0;
				return data_count;
			}

			else return 0;

			//line 57-61 yet to be edited
			//expect ACK from device
		}

		public int single_packet_send(ref Byte[] inputReportBuffer, int size, ref Byte[] outputBuffer) {
			//input command & size
			outputBuffer[0] = 0;
			outputBuffer[1] = 0x82;
			outputBuffer[2] = (byte)size;

			if(writeRawReportToDevice(outputBuffer) && readSingleReportFromDevice(ref inputReportBuffer)) {
				int data_count = (int)inputReportBuffer[1];
				inputReportBuffer[1] = 0;
				return data_count;
			}

			else return 0;
		}

		public int single_packet_poll_v2(ref Byte[] inputReportBuffer) {
            Byte[] outputBuffer = new Byte[65];

            outputBuffer[0] = 0;
            outputBuffer[1] = 0x81;

			if(writeRawReportToDevice(outputBuffer) && readSingleReportFromDevice(ref inputReportBuffer)) {
				int data_count = (int)inputReportBuffer[1];
				inputReportBuffer[1] = 0;
				return data_count;
			}	
			else return 0;
        }

		private int single_packet_poll_v2(ref Byte[] inputReportBuffer, ref byte[] outputBuffer) {
			if(writeRawReportToDevice(outputBuffer) && readSingleReportFromDevice(ref inputReportBuffer)) {
				int data_count = (int)inputReportBuffer[1];
				inputReportBuffer[1] = 0;
				return data_count;
			}
			else return 0;
		}

		public bool request_bdrate_change(ref Byte[] inBuffer, ref byte[] outBuffer, uint bdrate) {
			outBuffer[0] = 0;
			outBuffer[1] = 0x82;
			outBuffer[2] = (byte) (bdrate >> 8);
			outBuffer[3] = (byte) (bdrate & 0xff);

			if(single_packet_poll_v2(ref inBuffer, ref outBuffer) == 0x46) {
				return true;
			}
			else return false;
		}

		public bool single_packet_write_to_uart(ref Byte[] inputReportBuffer, ref byte[] outputBuffer) {
			outputBuffer[1] = 0x82;

			return false;
		}

		public int multi_packet_poll(ref Byte[] inputReportBuffer) {
			Byte[] outputBuffer = new Byte[65];

			outputBuffer[0] = 0;
			outputBuffer[1] = 0x82;

			if(writeRawReportToDevice(outputBuffer) && readMultipleReportsFromDevice(ref inputReportBuffer, 64)) {
				return inputReportBuffer[1];
			}
			return 0;
		}

        // Collect debug information from the device
        public String collectDebug() {
            // Collect debug information from USB device
            Debug.WriteLine("Reference Application -> Collecting debug information from device");

            // Declare our output buffer
            Byte[] outputBuffer = new Byte[65];

            // Declare our input buffer
            Byte[] inputBuffer = new Byte[65];

            // Byte 0 must be set to 0
            outputBuffer[0] = 0;

            // Byte 1 must be set to our command
            outputBuffer[1] = 0x10;

            // Send the collect debug command
            writeRawReportToDevice(outputBuffer);

            // Read the response from the device
            readSingleReportFromDevice(ref inputBuffer);

            // Byte 1 contains the number of characters transfered
            if (inputBuffer[1] == 0) return String.Empty;

            // Convert the Byte array into a string of the correct length
            string s = System.Text.ASCIIEncoding.ASCII.GetString(inputBuffer, 2, inputBuffer[1]);

            return s;
        }
    }
}
