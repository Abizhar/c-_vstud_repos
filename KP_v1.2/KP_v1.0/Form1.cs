﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using usbGenericHidCommunications;
using USB_Generic_HID_reference_application;

namespace KP_v1._0 {
	public partial class Form1 : Form {
		Byte[] data_in = new Byte[65];
		Byte[] data_out = new Byte[65];
		string[] separator = { " , ", " . ", " ", "\t", "\n" };
		uint[] baudrate = { 1200, 2400, 4800, 9600, 19200, 38400, 57600, 115200 };
		int separator_index = 0;
		int byte_counter = 0;
		bool USB_status = false;
		bool is_paused = false;
		bool data_waiting_to_send = false;
		string user_input;
		int interval;

		bool debug_receive_buffer_size = false, debug_send_buffer = false, debug_packet_marker = false;

		//usb reference device instance
		private usbReferenceDevice theReferenceUsbDevice;

		public Form1() {
			InitializeComponent();
			// Create the USB reference device object (passing VID and PID)
			theReferenceUsbDevice = new usbReferenceDevice(0x04D8, 0x0042);

			// Add a listener for usb events
			theReferenceUsbDevice.usbEvent += new usbReferenceDevice.usbEventsHandler(usbEvent_receiver);

			// Perform an initial search for the target device
			theReferenceUsbDevice.findTargetDevice();

			// load combo box initial index
			this.separator_comboBox.SelectedIndex = 0;

			// reload byte counter display
			this.byteReceived_lineEdit.Text = byte_counter.ToString();

			// iniitalize interval
			interval = this.dataPolling_timer.Interval;

			// initialize checkbox
			this.HEXMode_checkBox.Checked = true;
			this.sendOnEnter_checkBox.Checked = true;

			// display initial debug text
			this.debug_textBox.Text = "RS485 sniffer V1.0.0 \nProject KP juli - agustus 2018 \nInstitut Teknologi Sepuluh Nopember \n";
			if(theReferenceUsbDevice.isDeviceAttached)
				this.debug_textBox.AppendText("Device Connected \n");

			if(theReferenceUsbDevice.isDeviceAttached && USB_status) {
			}

		}

		private void Form1_Load(object sender, EventArgs e) {
		}

		//USB event callback
		private void usbEvent_receiver(object o, EventArgs e) {
			if(theReferenceUsbDevice.isDeviceAttached && !USB_status) {
				this.debug_textBox.AppendText("Device connected \n");
				USB_status = true;
			}

			else if(!theReferenceUsbDevice.isDeviceAttached && USB_status) {
				this.debug_textBox.AppendText("Device disconnected \n");
				USB_status = false;
			}
		}

		//Polling timer callback
		private void dataPolling_timer_Tick(object sender, EventArgs e) {
			string HEXstring, ASCIIstring;
			int data_count = 0;

			if(!data_waiting_to_send) { //jika tidak ada data yang perlu dikirim
				this.dataPolling_timer.Interval = interval;
				//if(theReferenceUsbDevice.isDeviceAttached && USB_status && !is_paused) {
				if(theReferenceUsbDevice.isDeviceAttached && !is_paused) {
					data_count = theReferenceUsbDevice.single_packet_poll_v2(ref data_in);
					if(data_count > 0) {
						byte_counter += data_count;
						this.byteReceived_lineEdit.Text = byte_counter.ToString();

						//convert array of byte to HEX string
						if(debug_packet_marker)
							this.HEX_textEdit.AppendText("  (S)");

						for(int i = 0; i < data_count; i++) {
							HEXstring = BitConverter.ToString(data_in, i + 2, 1);
							this.HEX_textEdit.AppendText(HEXstring);
							this.HEX_textEdit.AppendText(separator[separator_index]);
							if(data_in[i + 2] == 0) data_in[i + 2] = 0x20;
						}

						//convert array of byte to ASCII string
						ASCIIstring = System.Text.Encoding.ASCII.GetString(data_in, 2, data_count);
						this.ASCII_textEdit.AppendText(ASCIIstring);

					}
					if(debug_receive_buffer_size) {
						debug_textBox.AppendText(data_count + "\r\n");
					}
				}
			}

			else { // jika ada data yang perlu di kirim
				string user_input_temp;
				if(user_input.Length > 62 && !this.HEXMode_checkBox.Checked) { // jika input ASCII panjang
					this.dataPolling_timer.Interval = 100;
					user_input_temp = user_input.Substring(0, 62);
					data_count = theReferenceUsbDevice.single_packet_send(ref data_in, ref user_input_temp, 62, ref data_out);
					user_input = user_input.Remove(0, 62);

					//debug
					if(debug_send_buffer) {
						this.debug_textBox.AppendText(" ASCII_pendek \r\n");
						for(int i = 0; i < user_input_temp.Length; i++) {
							this.debug_textBox.AppendText(data_out[i].ToString());
							this.debug_textBox.AppendText(" ");
						}
						this.debug_textBox.AppendText("\r\n");
					}
				}
				else if(user_input.Length > 0 && (user_input.Length <= 62) && !this.HEXMode_checkBox.Checked) { //jika input ASCII pendek
					this.dataPolling_timer.Interval = 100;
					user_input_temp = user_input;
					data_count = theReferenceUsbDevice.single_packet_send(ref data_in, ref user_input_temp, user_input_temp.Length, ref data_out);
					data_waiting_to_send = false;

					//debug
					if(debug_send_buffer) {
						this.debug_textBox.AppendText(" ASCII_pendek \r\n");
						for(int i = 0; i < user_input_temp.Length; i++) {
							this.debug_textBox.AppendText(data_out[i].ToString());
							this.debug_textBox.AppendText(" ");
						}
						this.debug_textBox.AppendText("\r\n");
					}
					
				}
				else if(user_input.Length > 185 && this.HEXMode_checkBox.Checked) {// jika input HEX panjang
					this.dataPolling_timer.Interval = 100;
					user_input_temp = user_input.Substring(0, 185);
					//parsing HEX
					int input_count = theReferenceUsbDevice.HEXstringtoByte(ref user_input_temp, ref data_out);

					if(input_count < 0) {
						this.debug_textBox.AppendText("invalid input format \r\n");
						this.debug_textBox.AppendText("Example : \r\n aa ff 0f 0a 01 10 f3 \r\n");
						input_count = 0;
					}

					data_count = theReferenceUsbDevice.single_packet_send(ref data_in, input_count, ref data_out);
					user_input = user_input.Remove(0, 185);

					// debug
					if(debug_send_buffer) {
						this.debug_textBox.AppendText(" HEX_panjang \r\n");
						for(int j = 0; j < input_count + 3; j++) {
							this.debug_textBox.AppendText(data_out[j].ToString());
							this.debug_textBox.AppendText(" ");
						}
						this.debug_textBox.AppendText("\r\n");
					}
				}
				else if(user_input.Length > 0 && (user_input.Length <= 182) && this.HEXMode_checkBox.Checked) {//jika input HEX pendek
					this.dataPolling_timer.Interval = 100;
					user_input_temp = user_input;

					//parsing HEX
					int input_count = theReferenceUsbDevice.HEXstringtoByte(ref user_input_temp, ref data_out);

					if(input_count < 0) {
						this.debug_textBox.AppendText("invalid input format \r\n");
						this.debug_textBox.AppendText("Example : \r\n aa ff 0f 0a 01 10 f3 \r\n");
						input_count = 0;
					}

					data_count = theReferenceUsbDevice.single_packet_send(ref data_in, input_count, ref data_out);
					data_waiting_to_send = false;

					//debug
					if(debug_send_buffer) {
						this.debug_textBox.AppendText(" HEX_pendek \r\n");
						for(int j = 0; j < input_count + 3; j++) {
							this.debug_textBox.AppendText(data_out[j].ToString());
							this.debug_textBox.AppendText(" ");
						}
						this.debug_textBox.AppendText("\r\n");
					}
				}
				else {// jika data kosong
					data_waiting_to_send = false;
				}
			}
		}

		//Scroll button
		private void scrollToEnd_button_Click(object sender, EventArgs e) {
			this.HEX_textEdit.SelectionStart = this.HEX_textEdit.Text.Length;
			this.HEX_textEdit.ScrollToCaret();
			this.ASCII_textEdit.SelectionStart = this.ASCII_textEdit.Text.Length;
			this.ASCII_textEdit.ScrollToCaret();
		}

		//Autoscroll
		private void debug_textBox_TextChanged(object sender, EventArgs e) {
			this.debug_textBox.SelectionStart = this.debug_textBox.Text.Length;
			this.debug_textBox.ScrollToCaret();
		}

		private void HEX_textEdit_TextChanged(object sender, EventArgs e) {
			if(autoScroll_checkBox.Checked) {
				this.HEX_textEdit.SelectionStart = this.HEX_textEdit.Text.Length;
				this.HEX_textEdit.ScrollToCaret();
			}
		}

		private void ASCII_textEdit_TextChanged(object sender, EventArgs e) {
			if(autoScroll_checkBox.Checked) {
				this.ASCII_textEdit.SelectionStart = this.ASCII_textEdit.Text.Length;
				this.ASCII_textEdit.ScrollToCaret();
			}
		}

		private void ASCII_textEdit_MouseUp(object sender, MouseEventArgs e) {
			if(!is_paused) {
				this.ASCII_textEdit.Enabled = false;
				this.ASCII_textEdit.Enabled = true;
			}
		}

		private void HEX_textEdit_MouseUp_1(object sender, MouseEventArgs e) {
			if(!is_paused) {
				this.HEX_textEdit.Enabled = false;
				this.HEX_textEdit.Enabled = true;
			}
		}

		//separator character changed
		private void separator_comboBox_DropDownClosed(object sender, EventArgs e) {
			separator_index = separator_comboBox.SelectedIndex;
		}

		//clear display
		private void Clear_button_Click(object sender, EventArgs e) {
			this.HEX_textEdit.Clear();
			this.ASCII_textEdit.Clear();
			this.debug_textBox.AppendText("Display Cleared\n");
		}

		//reset data counter
		private void resetCounter_button_Click(object sender, EventArgs e) {
			this.debug_textBox.AppendText("Counter reset (" + byte_counter.ToString() + ") \n");
			byte_counter = 0;
			this.byteReceived_lineEdit.Text = byte_counter.ToString();
		}

		private void autoScroll_checkBox_CheckedChanged(object sender, EventArgs e) {

		}

		//Pause button click
		private void pause_button_Click(object sender, EventArgs e) {
			is_paused = !is_paused;

			if(is_paused) { //jika pause
				this.pause_button.Text = "Resume";
				this.HEX_textEdit.Enabled = true;
				this.ASCII_textEdit.Enabled = true;
			}

			else {//jika lanjut
				this.pause_button.Text = "Pause";
			}
		}

		//Send button handler
		private void send_button_Click(object sender, EventArgs e) {
			send_handler();
		}

		//textbox enter key handler
		private void send_textBox_KeyDown(object sender, KeyEventArgs e) {
			if(e.KeyCode == Keys.Enter && this.sendOnEnter_checkBox.Checked)
				send_handler();
		}
		private void send_textBox_KeyUp(object sender, KeyEventArgs e) {
			if(e.KeyCode == Keys.Enter && this.sendOnEnter_checkBox.Checked)
				this.send_textBox.Clear();
		}

		//send handler
		private void send_handler() {
			if(this.send_textBox.Text.Length > 2 && this.send_textBox.Text[0] == '#' && this.send_textBox.Text[0] == '#') { //command mode
				if(this.send_textBox.Text.Contains("##HELP")) {
					this.debug_textBox.AppendText("######### HELP PAGE ######### \r\n");
					this.debug_textBox.AppendText("##HELP \t\t\t display this help page \r\n");
					this.debug_textBox.AppendText("##POLL_INTERVAL ? \t display current polling interval (in milliseconds) \r\n");
					this.debug_textBox.AppendText("##POLL_INTERVAL = \t set polling interval (in milliseconds) \r\n");
					this.debug_textBox.AppendText("##CLEAR \t\t clear debug screen \r\n");
					this.debug_textBox.AppendText("##DEBUG_SEND_BUFFER = TRUE/FALSE \r\n");
					this.debug_textBox.AppendText("##DEBUG_RECEIVE_BUFFER_SIZE = TRUE/FALSE \r\n");
					this.debug_textBox.AppendText("##DEBUG_PACKET_MARKER = TRUE/FALSE \r\n");
				}
				else if(this.send_textBox.Text.Contains("##POLL_INTERVAL ?")) {
					this.debug_textBox.AppendText(this.dataPolling_timer.Interval.ToString() + "\r\n");
				}
				else if(this.send_textBox.Text.Contains("##POLL_INTERVAL = ")) {
					this.send_textBox.Text = this.send_textBox.Text.Remove(0, 18);
					if(int.TryParse(this.send_textBox.Text, out interval) && interval > 0) {
						this.dataPolling_timer.Interval = interval;
						this.debug_textBox.AppendText(this.dataPolling_timer.Interval.ToString() + "\r\n");
					}
					else
						this.debug_textBox.AppendText("invalid value \r\n");
				}
				else if(this.send_textBox.Text.Contains("##DEBUG_SEND_BUFFER = ")) {
					if(this.send_textBox.Text.Contains("TRUE")) {
						debug_send_buffer = true;
						this.debug_textBox.AppendText("debug_send_buffer = true \r\n");
					}

					else if(this.send_textBox.Text.Contains("FALSE")) {
						debug_send_buffer = false;
						this.debug_textBox.AppendText("debug_send_buffer = false \r\n");
					}

					else {
						this.debug_textBox.AppendText("invalid value \r\n");
					}
				}
				else if(this.send_textBox.Text.Contains("##DEBUG_RECEIVE_BUFFER_SIZE = ")) {
					if(this.send_textBox.Text.Contains("TRUE")) {
						debug_receive_buffer_size = true;
						this.debug_textBox.AppendText("debug_receive_buffer_size = true \r\n");
					}

					else if(this.send_textBox.Text.Contains("FALSE")) {
						debug_receive_buffer_size = false;
						this.debug_textBox.AppendText("debug_receive_buffer_size = false \r\n");
					}

					else {
						this.debug_textBox.AppendText("invalid value \r\n");
					}
				}
				else if(this.send_textBox.Text.Contains("##CLEAR")) {
					this.debug_textBox.Clear();
				}
				else if(this.send_textBox.Text.Contains("##DEBUG_PACKET_MARKER = ")) {
					if(this.send_textBox.Text.Contains("TRUE")) {
						debug_packet_marker = true;
						this.debug_textBox.AppendText("debug_packet_marker = true \r\n");
					}

					else if(this.send_textBox.Text.Contains("FALSE")) {
						debug_packet_marker = false;
						this.debug_textBox.AppendText("debug_packet_marker = false \r\n");
					}

					else {
						this.debug_textBox.AppendText("invalid value \r\n");
					}
				}
				else {
					this.debug_textBox.AppendText("command not found \r\n");
				}
				this.send_textBox.Clear();
			}
			else {
				user_input = this.send_textBox.Text;
				if(this.HEXMode_checkBox.Checked) {
					data_waiting_to_send = true;
					this.send_textBox.Clear();
				}

				else {
					data_waiting_to_send = true;
					this.send_textBox.Clear();
				}
				
			}		
		}
	}
}
